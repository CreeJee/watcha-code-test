
import { observable, action ,computed} from "mobx";
import {indexedArray,getRandomInt} from "../util.js"

let size = 5;
class BingoStore{
    lastestCell = null;
    @observable bingoCount = 0;
    @observable size = size;
    @observable checkedState = indexedArray(size).map(()=>indexedArray(size).map(()=>({text : "",checked : false})));
    @observable disabled = true ;

    getCellState(rowIndex,colIndex){
        return this.checkedState[rowIndex][colIndex];
    }
    @computed get clearedState(){
        let cleared = indexedArray(size ** 2,(v,k)=>k+1);

        return indexedArray(size).map((rowIndex)=>indexedArray(size).map(colIndex=>{
            return {
                checked : false, 
                text: cleared.splice(getRandomInt(0,cleared.length-1),1)[0],
                rowIndex,
                colIndex
            };
        }))
    }
    @computed get isWinner(){
        return this.bingoCount >= 5;
    }
    @action.bound
    checkCells(rowIndex,colIndex){
        if(!this.disabled){
            this.getCellState(rowIndex,colIndex).checked = true;
            this.addBingo(rowIndex,colIndex);
            this.lastestCell = this.getCellState(rowIndex,colIndex);
        }
    }
    @action.bound
    check(number){
        const $data = this.checkedState.flatMap((v,k,arr)=>v).find((node)=>node.text === number);
        this.checkCells($data.rowIndex,$data.colIndex);
    }
    @action.bound
    addBingo(rowIndex,colIndex){
        let lastIndex = this.size-1;
        let isSlash  = rowIndex === colIndex ;
        let isBackSlach = lastIndex-colIndex === rowIndex;
        let tempSizeArray = indexedArray(this.size);
        if(isSlash){
            this.bingoCount += tempSizeArray.every((index)=>this.getCellState(index,index).checked) ? 1 : 0;
        }
        if(isBackSlach){
            this.bingoCount += tempSizeArray.every((index)=>this.getCellState(index,lastIndex-index).checked) ? 1 : 0;
        }
        this.bingoCount += tempSizeArray.every((index)=>this.getCellState(rowIndex,index).checked) ? 1 : 0;
        this.bingoCount += tempSizeArray.every((index)=>this.getCellState(index,colIndex).checked) ? 1 : 0;
        
    }
    @action.bound
    reset(){
        this.disabled = false;
        this.checkedState = this.clearedState;
        this.bingoCount = 0;
    }
}
export default BingoStore;