import React from 'react';
import { observer, inject, Provider } from 'mobx-react';
import { action, computed } from 'mobx';
import "./Bingo.css";

import Cell from "./BingoCell.js";

@inject("bingoStore")
@inject("managerStore")
@observer
class Bingo extends React.Component{
    constructor({bingoStore,managerStore}){
        super();
        this.bingoStore = bingoStore;
        this.managerStore = managerStore;

    }
    @action.bound
    check(rowIndex,colIndex){
        if(!this.managerStore.isSelectedTurn(this.bingoStore)){
            alert("잘못된 차레입니다.");
        }
        else{
            let selectedCellText = this.getCellState(rowIndex,colIndex).text;
            this.managerStore.check(selectedCellText);
        }
    }
    getCellState(rowIndex,colIndex){
        return this.bingoStore.getCellState(rowIndex,colIndex);
    }
    
    render(){
        let {size,disabled,bingoCount,lastestCell} = this.bingoStore;
        let lengthObj = {length: size};
        return (
            <div className="bingo-table">
                {Array.from(
                    lengthObj,
                    (v,rowIndex) => Array.from(
                        lengthObj,
                        (v,colIndex)=>{
                            let cellState = this.getCellState(rowIndex,colIndex);
                            return (
                                <Provider bingoStore={this} key={colIndex}>
                                    <Cell 
                                        {...{rowIndex,colIndex}}
                                        size={size}
                                        text={disabled ? "" :cellState.text.toString()}
                                        checked={cellState.checked}
                                    ></Cell>
                                </Provider>
                            )
                        }
                    )
                )}
                <div>
                    <p>bingo Count : {bingoCount}</p>
                    <p>Lastest : {lastestCell === null ? "none" : lastestCell.text}</p>
                </div>
            </div>
        )
    }
}
export default Bingo;
