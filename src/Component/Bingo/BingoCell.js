import React from 'react';
import PropTypes from 'prop-types';
import { observable, action, computed } from "mobx";
import { observer ,inject } from 'mobx-react';

import "./BingoCell.css";
@inject("bingoStore")
@observer
class BingoCell extends React.Component{
    @action.bound
    check(){
        if(!this.isChecked){
            this.bingoStore.check(this.rowIndex,this.colIndex);
        }
    }
    constructor({colIndex,rowIndex,bingoStore}){
        super();
        
        this.colIndex = colIndex;
        this.rowIndex = rowIndex;
        this.bingoStore = bingoStore;
    }
    render(){
        let {text,checked,size} = this.props;

        return (    
            <div className={`cells ${checked ? "checked" : ""}`} onClick={this.check} style={{
                width : `${100/size}%`,
                height : `${100/size}%`
            }}>
                <span className="text">{text}</span>
            </div>
        )
    }
}
BingoCell.defaultProps = {
    checked : false,
}
BingoCell.propTypes = {
    rowIndex :PropTypes.number.isRequired,
    colIndex : PropTypes.number.isRequired,
    text : PropTypes.string.isRequired,
    size : PropTypes.number.isRequired,
    checked : PropTypes.bool.isRequired,
};
export default BingoCell;
