import React from 'react';
import { Provider, observer } from 'mobx-react';
import { observable,computed,action } from 'mobx';

import BingoStore from "./Bingo/BingoStore.js";
import Bingo from "./Bingo/Bingo.js";
import {indexedArray} from "./util.js"
import './App.css';

@observer
class App extends React.Component{
  registerdPlayers = [];
  playerTurnCount = 0;
  @observable isStart = false;
  @computed get startButtonText(){
    return this.isStart ? "게임 재시작" : "시작";
  }
  @action.bound 
  startGame(){
    if(!this.isStart){
      this.isStart = true;
    }
    for(const bingoStore of this.registerdPlayers){
      bingoStore.reset();
    }
  }
  @action.bound
  endGame(){
    this.isStart = false;
    for(const bingoStore of this.registerdPlayers){
      bingoStore.reset();
    }
  }

  @action.bound
  check(text){
    ++this.playerTurnCount;
    const winners = [];
    const playersEntries = this.registerdPlayers.entries();
    for(let [index,player] of playersEntries){
      if(!player.disabled){
        player.check(text);
      }
      if(player.isWinner){
        winners.push(index);
      }
    }
    if(winners.length > 0){
      let winnerMessage = winners.map(v=>`${v+1}P`).join(",");
      if(winners.length > 1){
        winnerMessage += `가 무승부입니다.`
      }
      else{
        winnerMessage += `가 빙고를 완성했습니다.`
      }
      alert(winnerMessage);
      this.endGame();
    }
  }
  isSelectedTurn(self){
    let selectedIndex = this.registerdPlayers.indexOf(self);
    if(selectedIndex === -1){
        return false;
    }
    return (this.playerTurnCount)%this.registerdPlayers.length === selectedIndex;
  }


  constructor(){
    super();
    this.bingoBoards = indexedArray(2).map((index)=>(
      <div className="block" key={index}>
        <Provider 
          bingoStore={this.registerdPlayers[index] = new BingoStore()}
          managerStore={this}
        >
          <Bingo></Bingo>
        </Provider>
      </div>)
    )
  }
  render(){
    return (
      <div className="App">
        {this.bingoBoards}
        <div className="large">
          <button id="start-btn" onClick={this.startGame}>{this.startButtonText}</button>
        </div>
      </div>
    );
  }
}

export default App;
