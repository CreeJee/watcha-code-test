export const getRandomInt = (min, max) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
/**
 * Callback for mapping variable
 *
 * @callback MapperCallback
 * @param {Any} v - A value.
 * @param {index} k - A key.
 */
 /**
 * 
 * @param {Number} size 
 * @param {MapperCallback} mapper
 */
export const indexedArray = (size,mapper = (v,k)=>k)=>Array.from({length : size},mapper);